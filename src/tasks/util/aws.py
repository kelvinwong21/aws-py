import boto3

from tasks.util.menu_item import MenuItem


def init_aws_client(profile, resource):
    """
    Initiates a new AWS client for cognito-identity
    """

    session = boto3.Session(profile_name=profile)
    client = session.client(resource)

    return client


def get_current_region():
    """
    Get a boto3 session
    """

    session = boto3.session.Session()

    return session.region_name


def map_key_values_for_menu(aws_dict, awsKey, awsName):
    """
    Creates a dict for displaying a menu
    """

    dict_menu_items = dict()
    counter = 1

    for item in aws_dict:
        key_pair_menu_item = MenuItem(item.get(awsKey), item.get(awsName))
        dict_menu_items[counter] = key_pair_menu_item
        counter += 1

    return dict_menu_items
