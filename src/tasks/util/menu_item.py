class MenuItem:
    """
    Helper class to display a menu
    """
    def __init__(self, aws_key, friendly_name=""):
        self.aws_key = aws_key
        self.friendly_name = friendly_name
